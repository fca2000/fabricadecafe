-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler version: 0.9.4
-- PostgreSQL version: 13.0
-- Project Site: pgmodeler.io
-- Model Author: ---

-- Database creation must be performed outside a multi lined SQL file. 
-- These commands were put in this file only as a convenience.
-- 
-- object: "EMBALAGEM" | type: DATABASE --
-- DROP DATABASE IF EXISTS "EMBALAGEM";
CREATE DATABASE "EMBALAGEM";
-- ddl-end --


-- object: public."MATERIAPRIMA" | type: TABLE --
-- DROP TABLE IF EXISTS public."MATERIAPRIMA" CASCADE;
CREATE TABLE public."MATERIAPRIMA" (
	id_materia_prima smallint NOT NULL,
	fornecedor smallint,
	"tipo de grão" smallint,
	aroma smallint,
	CONSTRAINT "MATERIAPRIMA_pk" PRIMARY KEY (id_materia_prima)
);
-- ddl-end --
ALTER TABLE public."MATERIAPRIMA" OWNER TO postgres;
-- ddl-end --

-- object: public."PROCESSO 1" | type: TABLE --
-- DROP TABLE IF EXISTS public."PROCESSO 1" CASCADE;
CREATE TABLE public."PROCESSO 1" (
	id_processo_1 smallint NOT NULL,
	id_materia_prima smallint,
	data_processo smallint,
	tipo_processo smallint,
	"id_materia_prima_MATERIAPRIMA" smallint,
	CONSTRAINT "PROCESSO 1_pk" PRIMARY KEY (id_processo_1)
);
-- ddl-end --
ALTER TABLE public."PROCESSO 1" OWNER TO postgres;
-- ddl-end --

-- object: public."PROCESSO 2" | type: TABLE --
-- DROP TABLE IF EXISTS public."PROCESSO 2" CASCADE;
CREATE TABLE public."PROCESSO 2" (
	id_processo_2 smallint NOT NULL,
	id_processo_1 smallint,
	data_processo smallint,
	tipo_processo smallint,
	temperatura_processo smallint,
	CONSTRAINT "PROCESSO 2_pk" PRIMARY KEY (id_processo_2)
);
-- ddl-end --
ALTER TABLE public."PROCESSO 2" OWNER TO postgres;
-- ddl-end --

-- object: "MATERIAPRIMA_fk" | type: CONSTRAINT --
-- ALTER TABLE public."PROCESSO 1" DROP CONSTRAINT IF EXISTS "MATERIAPRIMA_fk" CASCADE;
ALTER TABLE public."PROCESSO 1" ADD CONSTRAINT "MATERIAPRIMA_fk" FOREIGN KEY ("id_materia_prima_MATERIAPRIMA")
REFERENCES public."MATERIAPRIMA" (id_materia_prima) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: "PROCESSO 1_uq" | type: CONSTRAINT --
-- ALTER TABLE public."PROCESSO 1" DROP CONSTRAINT IF EXISTS "PROCESSO 1_uq" CASCADE;
ALTER TABLE public."PROCESSO 1" ADD CONSTRAINT "PROCESSO 1_uq" UNIQUE ("id_materia_prima_MATERIAPRIMA");
-- ddl-end --

-- object: public."many_PROCESSO 1_has_many_PROCESSO 2" | type: TABLE --
-- DROP TABLE IF EXISTS public."many_PROCESSO 1_has_many_PROCESSO 2" CASCADE;
CREATE TABLE public."many_PROCESSO 1_has_many_PROCESSO 2" (
	"id_processo_1_PROCESSO 1" smallint NOT NULL,
	"id_processo_2_PROCESSO 2" smallint NOT NULL,
	CONSTRAINT "many_PROCESSO 1_has_many_PROCESSO 2_pk" PRIMARY KEY ("id_processo_1_PROCESSO 1","id_processo_2_PROCESSO 2")
);
-- ddl-end --

-- object: "PROCESSO 1_fk" | type: CONSTRAINT --
-- ALTER TABLE public."many_PROCESSO 1_has_many_PROCESSO 2" DROP CONSTRAINT IF EXISTS "PROCESSO 1_fk" CASCADE;
ALTER TABLE public."many_PROCESSO 1_has_many_PROCESSO 2" ADD CONSTRAINT "PROCESSO 1_fk" FOREIGN KEY ("id_processo_1_PROCESSO 1")
REFERENCES public."PROCESSO 1" (id_processo_1) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: "PROCESSO 2_fk" | type: CONSTRAINT --
-- ALTER TABLE public."many_PROCESSO 1_has_many_PROCESSO 2" DROP CONSTRAINT IF EXISTS "PROCESSO 2_fk" CASCADE;
ALTER TABLE public."many_PROCESSO 1_has_many_PROCESSO 2" ADD CONSTRAINT "PROCESSO 2_fk" FOREIGN KEY ("id_processo_2_PROCESSO 2")
REFERENCES public."PROCESSO 2" (id_processo_2) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --


